<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/form', 'AuthController@form');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-table', 'IndexController@table');

// CRUD CAST

// Create
Route::get('/cast/create', 'CastController@create'); //Route menuju form create
Route::post('/cast', 'CastController@store'); //Route untuk menyimpan ke database

//Read
Route::get('/cast', 'CastController@index'); //Route list cast
Route::get('/cast/{cast_id}', 'CastController@show'); //Route detail cast

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route menuju form edit
Route::put('/cast/{cast_id}', 'CastController@update'); //Route update data berdasarkan ID

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route hapus data
Route::put('/cast/{cast_id}', 'CastController@update'); //Route update data berdasarkan ID
