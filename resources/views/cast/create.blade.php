@extends('layout.master')
@section('title')
Tambah Cast
@endsection
@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" name="nama" class="form-control" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input name="umur" class="form-control" placeholder="Masukkan umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control" placeholder="Masukkan bio"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @endsection