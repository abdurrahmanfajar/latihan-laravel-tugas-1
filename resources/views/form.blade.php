@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
    <h2>Buat Akun Baru</h2>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label>
        <br>
        <input type="text" name="namaDepan"><br>
        <br>
        <label>Last name:</label>
        <br>
        <input type="text" name="namaBelakang">
        <br><br>
        <label>Gender:</label>
        <br>
        <input type="radio" name="Male">
        <label>Male</label>
        <br>
        <input type="radio" name="Female">
        <label>Female</label>
        <br><br>
        <label>Nationality</label><br>
        <select>
            <option value="Indonesia">Indonesia</option>
            <option value="Palestina">Palestina</option>
            <option value="Saudi Arabia">Saudi Arabia</option>
        </select>
        <br><br>
        <label>Language Spoken</label><br>
            <input type="checkbox">
            <label>Bahasa Indonesia</label><br>
            <input type="checkbox">
            <label>English</label><br>
            <input type="checkbox">
            <label>Other</label>
        <br><br>
        <label>Bio</label><br>
        <textarea cols="30" rows="5" name="biodata"></textarea>
        <br>
        <input type="submit" value="Sign Up">
      </form> 
      @endsection