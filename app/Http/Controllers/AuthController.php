<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $welcome){
        $namaDepan = $welcome['namaDepan'];
        $namaBelakang = $welcome['namaBelakang'];
        return view('welcome', compact('namaDepan','namaBelakang'));
    }
}
